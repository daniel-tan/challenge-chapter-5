const express = require('express')
const app = express()
const port = 3000
const fs = require('fs')
const AUTH_PATH = './views/auth.json';
app.set("view engine", "ejs");

// ? termasuk middleware?
app.use(express.static(__dirname + "/assets"));
app.use(express.static(__dirname + "/views"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
     res.render('index');
});

app.get('/login', (req, res) => {
     res.render('login', {
          message : ""
     });
})

app.post('/process', (req, res) => {
     const { email, password } = req.body;
     fs.readFile(AUTH_PATH, "utf8", (err, data) => {
          if (err) {
               console.log("Error reading file, ", err)
          } else {
               const auth = JSON.parse(data);
               if ((auth.email === email) && (auth.password === password)) {
                    console.log('login')
                    res.render('game')
               } else {
                    console.log("cek data masuk = ", email, password);
                    // console.log('fail')
                    res.render('login', {
                         message: "ID atau Password incorrect, please try again"
                    })
               }
          }
     });
});

app.get('/game', (req, res) => {
     res.render('game');
})


app.listen(port, () => console.log(`Example app listening on port ${port}!`))